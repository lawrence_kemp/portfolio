package us.kemp.lawrence.portfolio.metro;

import java.util.ArrayList;
import java.util.List;

public class Passenger extends Thread implements Comparable<Passenger> {

	public final String name;
	public Station boardStation;
	private List<Station> itinerary;
	
	public Passenger(String name, Station start, List<Station> itinerary) {
		this.name = name;
		this.boardStation = start;
		this.itinerary = itinerary;
	}
	
	public int compareTo(Passenger passenger) {
		if (passenger == this) {
			return 0;
		}
		
		int c = this.name.compareToIgnoreCase(passenger.name);
		
		if (c != 0) {
			return c;
		}
		
		throw new Error("Two different passenger objects with the same name");
	}

	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public void run() {
		boolean active = true;
		SimulationEvent event = null;
		Station currentStation = null;
		Station destinationStation = null;
		Train currentTrain = null;
		// First, let's go to the starting station
		destinationStation = enterStationQueue(boardStation);
		currentStation = boardStation;
		while (active) {
			// Are we waiting for a train?
			if (currentStation != null) {
				synchronized(Simulation.trainsAtStations) {
					if (Simulation.trainsAtStations.containsKey(currentStation)) {
						currentTrain = Simulation.trainsAtStations.get(currentStation).get(0);
						if (currentTrain.line.indexOf(destinationStation) > 0) {
							enterTrainAtStation(currentStation, currentTrain);
							currentStation = null;
							Simulation.trainsAtStations.notifyAll();
						} else {
							try {
								Simulation.trainsAtStations.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					} else {
						try {
							Simulation.trainsAtStations.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			} else {
				if (currentTrain != null && destinationStation != null) {
					// Waiting for train to arrive at destination
					synchronized(Simulation.trainsAtStations) {
						if (Simulation.trainsAtStations.containsKey(destinationStation)) {
							List<Train> trains = Simulation.trainsAtStations.get(destinationStation);
							if (trains.contains(currentTrain)) {
								synchronized(Simulation.passOnTrains) {
									List<Passenger> passengers = Simulation.passOnTrains.get(currentTrain);
									passengers.remove(this);
									
									if (passengers.isEmpty()) {
										Simulation.passOnTrains.remove(currentTrain);
									} else {
										Simulation.passOnTrains.put(currentTrain, passengers);
									}
									event = SimulationEvent.passengerAlight(this, currentTrain, destinationStation);
									System.out.println(event.toString());
									System.out.flush();
								}
								
								this.itinerary.remove(destinationStation);
								
								if (!this.itinerary.isEmpty()) {
									currentStation = destinationStation;
									destinationStation = enterStationQueue(currentStation);
								}
								currentTrain = null;
								Simulation.trainsAtStations.notifyAll();
							} else {
								try {
									Simulation.trainsAtStations.wait();
								} catch (InterruptedException e) {
									e.printStackTrace();
								}	
							}
						} else {
							try {
								Simulation.trainsAtStations.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}							
						}
					}
				}
			}
			
			if (this.itinerary.isEmpty()){
				active = false;
			}
		}
	}

	private void enterTrainAtStation(Station currentStation, Train currentTrain) {
		SimulationEvent event;
		synchronized(Simulation.passOnTrains) {
			List<Passenger> passengers = new ArrayList<Passenger>();
			if (Simulation.passOnTrains.containsKey(currentTrain)) {
				passengers = Simulation.passOnTrains.get(currentTrain);
			}
			passengers.add(this);
			Simulation.passOnTrains.put(currentTrain, passengers);
			event = SimulationEvent.passengerBoard(this, currentTrain, currentStation);
			System.out.println(event.toString());
			System.out.flush();
			Simulation.passOnTrains.notifyAll();
		}
		synchronized(Simulation.passAtStations) {
			List<Passenger> passengers = Simulation.passAtStations.get(currentStation);
			passengers.remove(this);
			if (passengers.isEmpty()) {
				Simulation.passAtStations.remove(currentStation);
			} else {
				Simulation.passAtStations.put(currentStation, passengers);
			}
			Simulation.passAtStations.notifyAll();
		}
	}

	private Station enterStationQueue(Station currentStation) {
		Station destinationStation = null;
		synchronized(Simulation.passAtStations){
			List<Passenger> passengers = new ArrayList<Passenger>();
			if (Simulation.passAtStations.containsKey(currentStation)) {
				passengers = Simulation.passAtStations.get(currentStation);
			}
			passengers.add(this);
			Simulation.passAtStations.put(currentStation, passengers);
			Simulation.passAtStations.notifyAll();
			destinationStation = this.itinerary.get(0);
		}
		
		return destinationStation;
	}
}
