package us.kemp.lawrence.portfolio.metro;

import java.util.ArrayList;
import java.util.List;

public class TrainLine implements Comparable<TrainLine> {

	public final String name;
	private final List<Station> stations;
	
	public TrainLine(String name) {
		this.name = name;
		stations = new ArrayList<Station>();
	}
	
	public void addStation(Station station) {
		stations.add(station);
	}
	
	public int numStations() {
		return stations.size();
	}
	
	public int indexOf(Station station) {
		return stations.indexOf(station);
	}
	
	public Station stationAt(int index) {
		return stations.get(index);
	}
	
	public int compareTo(TrainLine line) {
		if (line == this) {
			return 0;
		}
		
		int c = this.name.compareToIgnoreCase(line.name);
		
		if (c != 0) {
			return c;
		}
		
		throw new Error("Two different TrainLine objects with same name");
	}

	@Override
	public String toString() {
		return name;
	}
}
