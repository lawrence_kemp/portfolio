package us.kemp.lawrence.portfolio.metro;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This project is adapted from a University of Maryland, College Park
 * CMSC 330 (Organiation of Programming Languages) project 
 * 
 * It can be found at: http://www.cs.umd.edu/class/spring2010/cmsc330/p5/
 *
 */
public class Simulation {
	private static final char SIMULATION_FILE_COMMENT_MARKER = '%';

	private static final String SIMULATION_FILE_OUTPUT_MARKER = "=== Output ===";

	private static final String SIMULATION_FILE_PASSENGER_MARKER = "=== Passengers ===";

	private static final String SIMULATION_FILE_TRAIN_MARKER = "=== Trains ===";

	private static final String SIMULATION_FILE_LINE_MARKER = "=== Lines ===";

	private enum ParsePhase {
		START,
		LINES,
		TRAINS,
		PASSENGERS,
		OUTPUT
	};
	
	private static Pattern pTrainEnter = Pattern.compile("Train (\\w+) (\\d+) entering ([\\w '-]+)");
	
	private static Pattern pTrainExit = Pattern.compile("Train (\\w+) (\\d+) leaving ([\\w '-]+)");
	
	private static Pattern pPassengerBoard = Pattern.compile("([\\w-]+) boarding train (\\w+) (\\d+) at ([\\w '-]+)");
	
	private static Pattern pPassengerAlight = Pattern.compile("([\\w-]+) leaving train (\\w+) (\\d+) at ([\\w '-]+)");
	
	private static List<TrainLine> lines = null;
	private static List<Passenger> passengers = null;
	private static List<Train> trains = null;
	private static List<Station> stations = null;
	private static List<SimulationEvent> events = null;
	
	public static Map<Station,List<Train>> trainsAtStations = null;
	public static Map<Station,List<Passenger>> passAtStations = null;
	public static Map<Train,List<Passenger>> passOnTrains = null;
	
	private static void initSimulation() {
		lines = new ArrayList<TrainLine>();
		passengers = new ArrayList<Passenger>();
		trains = new ArrayList<Train>();
		stations = new ArrayList<Station>();
		events = new ArrayList<SimulationEvent>();
	}
	
	private static Station findStation(String name) {
		for (Station s: stations) {
			if (s.name.equals(name)) {
				return s;
			}
		}
		return null;
	}
	
	private static TrainLine findTrainLine(String lineName) {
		for (TrainLine line : lines) {
			if (line.name.equals(lineName)) {
				return line;
			}
		}
		return null;
	}
	
	private static Train findTrain(TrainLine line, int num) {
		for (Train train : trains) {
			if (train.line == line && train.number == num) {
				return train;
			}
		}
		return null;
	}
	
	private static Passenger findPassenger(String passengerName) {
		for (Passenger passenger: passengers) {
			if (passenger.name.equals(passengerName)) {
				return passenger;
			}
		}
		return null;
	}
	
	public static boolean parseFile(String file) {
		initSimulation();
		boolean ok = true;
		ParsePhase phase = ParsePhase.START;
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String fileLine;
			
			while((fileLine = reader.readLine()) != null) {
				if (fileLine.equals(SIMULATION_FILE_LINE_MARKER)) {
					phase = ParsePhase.LINES;
				} else if (fileLine.equals(SIMULATION_FILE_TRAIN_MARKER)) {
					phase = ParsePhase.TRAINS;
				} else if (fileLine.equals(SIMULATION_FILE_PASSENGER_MARKER)) {
					phase = ParsePhase.PASSENGERS;
				} else if (fileLine.equals(SIMULATION_FILE_OUTPUT_MARKER)) {
					phase = ParsePhase.OUTPUT;
				} else {
					if (fileLine.length() == 0 || fileLine.charAt(0) == SIMULATION_FILE_COMMENT_MARKER) {
						continue; // either a comment or an empty line
					}
					if (phase == ParsePhase.LINES) {
						String[] strings = fileLine.split(",[ ]*");
						TrainLine line = new TrainLine(strings[0]);
						for(int index = 1; index < strings.length; index++) {
							String stationName = strings[index];
							Station station = findStation(stationName);
							if (station == null) {
								station = new Station(stationName);
								stations.add(station);
							}
							line.addStation(station);
						}
						lines.add(line);
					} else if (phase == ParsePhase.TRAINS) {
						String[] strings = fileLine.split("=");
						String lineName = strings[0];
						int num = Integer.decode(strings[1]).intValue();
						TrainLine trainLine = null;
						for (TrainLine line : lines) {
							if (line.name.equals(lineName)) {
								trainLine = line;
								break;
							}
						}
						for (int index = 0; index < num; index++) {
							trains.add(new Train(trainLine, index + 1));
						}
					} else if (phase == ParsePhase.PASSENGERS) {
						String[] strings = fileLine.split(",[ ]*");
						String name = strings[0];
						Station start = findStation(strings[1]);
						ArrayList<Station> itinerary = new ArrayList<Station>();
						for (int index = 2; index < strings.length; index++) {
							itinerary.add(findStation(strings[index]));
						}
						passengers.add(new Passenger(name, start, itinerary));
					} else if (phase == ParsePhase.OUTPUT) {
						Matcher m;
						if ((m = pTrainEnter.matcher(fileLine)).matches()) {
							String lineName = m.group(1);
							String trainNum = m.group(2);
							int num = Integer.decode(trainNum).intValue();
							String stationName = m.group(3);
							TrainLine line = findTrainLine(lineName);
							Train train = findTrain(line,num);
							Station station = findStation(stationName);
							SimulationEvent e = SimulationEvent.trainArrival(train, station);
							events.add(e);
						} else if ((m = pTrainExit.matcher(fileLine)).matches()) {
							String lineName = m.group(1);
							String trainNum = m.group(2);
							int num = Integer.decode(trainNum).intValue();
							String stationName = m.group(3);
							TrainLine line = findTrainLine(lineName);
							Train train = findTrain(line,num);
							Station station = findStation(stationName);
							SimulationEvent e = SimulationEvent.trainDeparture(train, station);
							events.add(e);
						} else if ((m = pPassengerBoard.matcher(fileLine)).matches()) {
							String passengerName = m.group(1);
							String lineName = m.group(2);
							String trainNum = m.group(3);
							int num = Integer.decode(trainNum).intValue();
							String stationName = m.group(4);
							TrainLine line = findTrainLine(lineName);
							Train train = findTrain(line,num);
							Station station = findStation(stationName);
							Passenger passenger = findPassenger(passengerName);
							SimulationEvent e = SimulationEvent.passengerBoard(passenger, train, station);
							events.add(e);
						} else if ((m = pPassengerAlight.matcher(fileLine)).matches()) {
							String passengerName = m.group(1);
							String lineName = m.group(2);
							String trainNum = m.group(3);
							int num = Integer.decode(trainNum).intValue();
							String stationName = m.group(4);
							TrainLine line = findTrainLine(lineName);
							Train train = findTrain(line,num);
							Station station = findStation(stationName);
							Passenger passenger = findPassenger(passengerName);
							SimulationEvent e = SimulationEvent.passengerAlight(passenger, train, station);
							events.add(e);
						} else {
							// Malformed output line
							System.out.println("Malformed output line: " + fileLine);
							ok = false;
						}
					} else {
						// Skipping
						System.out.println("Skipping: " + fileLine);
						ok = false;
					}
				}
			}
			
			return ok;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private static void displayState(Map<Station,List<Train>> trainsAtStations, Map<Station,List<Passenger>> passAtStations, Map<Train,List<Passenger>> passOnTrains) {
		Collections.sort(lines);
		for(TrainLine line : lines) {
			System.out.println(line);
			int nStations = line.numStations();
			for (int i = 0; i < nStations; i++) {
				Station station = line.stationAt(i);
				
				List<Passenger> ps = passAtStations.get(station);
				String pStr = "";
				if (ps != null) {
					Collections.sort(ps);
					for (Passenger p : ps) {
						pStr = pStr + p + " ";
					}
				}
				
				List<Train> ts = trainsAtStations.get(station);
				String tStr = "";
				if (ts != null) {
					Collections.sort(ts);
					for (Train train : ts) {
						if (train.line.name.equals(line.name)) {
							tStr = tStr + "[" + train;
							List<Passenger> pts = passOnTrains.get(train);
							if (pts != null) {
								Collections.sort(pts);
								for (Passenger p : pts) {
									tStr = tStr + " " + p;
								}
							}
							tStr = tStr + "]";
						}
					}
				}
				
				System.out.println("  " + station + "     " + pStr + " " + tStr);
			}
		}
		System.out.println("");
	}
	
	private static void display() {
		trainsAtStations = new HashMap<Station,List<Train>>();
		passAtStations = new HashMap<Station,List<Passenger>>();
		passOnTrains = new HashMap<Train,List<Passenger>>();
		
		//TODO: First, initialize station->passenger map, then display the initial state, as follows
		for(Passenger passenger: passengers) {
			List<Passenger> waiting = new ArrayList<Passenger>();
			if (passAtStations.containsKey(passenger.boardStation)) {
				waiting = passAtStations.get(passenger.boardStation);
				waiting.add(passenger);
			} else {
				waiting.add(passenger);
			}
			passAtStations.put(passenger.boardStation, waiting);
		}
		
		displayState(trainsAtStations, passAtStations, passOnTrains);
		
		//TODO: Second, iterate through all gathered events, update the maps, and call displayState
		//System.out.println("DISPLAY not implemented");
		for (SimulationEvent e : events) {
			List<Train> trains = new ArrayList<Train>();
			List<Passenger> waiting = new ArrayList<Passenger>();
			List<Passenger> riding = new ArrayList<Passenger>();
			
			switch (e.event){
			case TrainArrival:
				if (trainsAtStations.isEmpty() || !trainsAtStations.containsKey(e.station)) {
					trains.add(e.train);
				} else {
					trains = trainsAtStations.get(e.station);
					if (!trains.contains(e.train)) {
						trains.add(e.train);
					}
				}
				trainsAtStations.put(e.station, trains);
				break;
			case TrainDeparture:
				if (trainsAtStations.containsKey(e.station)) {
					trains = trainsAtStations.get(e.station);
					if (trains.contains(e.train)) {
						trains.remove(e.train);
					}
				}
				if (trains.isEmpty()) {
					trainsAtStations.remove(e.station);
				} else {
					trainsAtStations.put(e.station, trains);
				}
				break;
			case PassengerBoard:
				if (passAtStations.containsKey(e.station)) {
					waiting = passAtStations.get(e.station);
					if (waiting.contains(e.passenger)) {
						waiting.remove(e.passenger);
					}
				}
				passAtStations.put(e.station, waiting);
				
				if (passOnTrains.containsKey(e.train)) {
					riding = passOnTrains.get(e.train);
					if (!riding.contains(e.passenger)) {
						riding.add(e.passenger);
					}
				} else {
					riding.add(e.passenger);
				}
				passOnTrains.put(e.train, riding);
				break;
			case PassengerAlight:
				if (passOnTrains.containsKey(e.train)) {
					riding = passOnTrains.get(e.train);
					if (riding.contains(e.passenger)) {
						riding.remove(e.passenger);
					}
				} else {
					riding.add(e.passenger);
				}
				passOnTrains.put(e.train, riding);

				if (passAtStations.containsKey(e.station)) {
					waiting = passAtStations.get(e.station);
					if (!waiting.contains(e.passenger)) {
						waiting.add(e.passenger);
					}
				}
				passAtStations.put(e.station, waiting);
				break;
			default:
				break;
			}
			System.out.println(e.toString());
			displayState(trainsAtStations, passAtStations, passOnTrains);
		}
	}
	
	private static void simulate() {
		trainsAtStations = new HashMap<Station,List<Train>>();
		passAtStations = new HashMap<Station,List<Passenger>>();
		passOnTrains = new HashMap<Train,List<Passenger>>();
		//System.out.println("SIMULATION not implemented");
		for(Passenger passenger : passengers) {
			passenger.start();
		}
		for(Train train : trains) {
			train.start();
		}
	}
	
	public static void printParams(String file) {
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String fileLine;
			while((fileLine = reader.readLine()) != null) {
				if (fileLine.equals(SIMULATION_FILE_OUTPUT_MARKER)) {
					break;
				} else if (fileLine.length() == 0 || fileLine.charAt(0) == '%') {
					continue;
				}
				System.out.println(fileLine);
			}
			System.out.println(SIMULATION_FILE_OUTPUT_MARKER);
			System.out.flush();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	public static void main(String args[]) {
		if (args.length != 2) {
			System.out.println("Usage: java Simulation [simulate|display] <simFileName>");
			System.exit(1);
		}
		String filePath = findFile(args);
		
		if (!parseFile(filePath)) {
			System.err.println("Input file failed to parse!\n");
		} else {
			if (args[0].equals("simulate")) {
				printParams(filePath); // show simulation parameters before simulation output
				simulate();
			} else if (args[0].equals("display")) {
				display();
			}
		}
	}

	private static String findFile(String[] args) {
		String filePath = args[1];
		File file = new File(filePath);
		if (!file.exists()){
			try {
				file = new File(Thread.currentThread().getContextClassLoader().getResource(filePath).toURI());
				if (file.exists()) {
					filePath = file.getPath();
				}
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
		}
		return filePath;
	}
}
