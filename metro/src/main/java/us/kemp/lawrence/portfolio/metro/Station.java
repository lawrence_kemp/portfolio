package us.kemp.lawrence.portfolio.metro;

public class Station implements Comparable<Station> {

	public final String name;
	
	public Station(String name) {
		this.name = name;
	}
	
	public int compareTo(Station station) {
		if (station == this) {
			return 0;
		}
		int c = this.name.compareToIgnoreCase(station.name);
		if (c != 0) {
			return c;
		}
		throw new Error("Two different Station objects with the same name");
	}

	@Override
	public String toString() {
		return name;
	}
}
