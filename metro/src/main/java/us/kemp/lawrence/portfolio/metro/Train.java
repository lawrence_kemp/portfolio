package us.kemp.lawrence.portfolio.metro;

import java.util.ArrayList;
import java.util.List;

public class Train extends Thread implements Comparable<Train> {

	public final TrainLine line;
	public final int number;
	
	public Train(TrainLine line, int number) {
		this.line = line;
		this.number = number;
	}
	
	public int compareTo(Train train) {
		if (train == this) {
			return 0;
		}
		
		int c = this.line.compareTo(train.line);
		
		if (c != 0) {
			return c;
		}
		
		if (this.number == train.number) {
			throw new Error("Two Train objects with equal name and line but not the same number");
		} else if (this.number > train.number) {
			return 1;
		} else {
			return -1;
		}
	}

	@Override
	public String toString() {
		return this.line + " " + this.number;
	}

	@Override
	public void run() {
		/*
		 * Let's talk about the different train states:
		 * 1. Entering the train line (initial state)
		 * In this state, the train is about to enter the train line at the first station.
		 * 
		 * 2. Entering a station
		 * In this state, the train is entering a station on the train line.
		 * Once the train has entered, it will pause for some time to allow passengers to board.
		 * 
		 * 3. Leaving a station
		 * In this state, the train is leaving a station on the train line.
		 * Here is where we determine the next station
		 * 
		 * 4. Leaving the train line (final state)
		 * In this state, the train is leaving the train line. It has to leave from the first station.
		 * Once the train has left the train line, operations cease for the train.
		 */
		
		boolean active = true;
		boolean forward = true;
		Station currentStation = null;
		Station priorStation = null;
		SimulationEvent event = null;
		do {
			if (currentStation == null) {
				if (priorStation == null) {
					// Initial state
					currentStation = line.stationAt(0);
				} else {
					// Entering a station
					forward = calculateTrainDirection(forward, priorStation);
					
					if (forward){
						currentStation = line.stationAt(line.indexOf(priorStation) + 1);
					} else {
						currentStation = line.stationAt(line.indexOf(priorStation) - 1);
					}
				}
				// Wait for station to be available for line.
				synchronized(Simulation.trainsAtStations) {
					boolean otherTrainsForLineAtStation;
					List<Train> trains = new ArrayList<Train>();
					do {
						otherTrainsForLineAtStation = false;
						if (Simulation.trainsAtStations.containsKey(currentStation)) {
							trains = Simulation.trainsAtStations.get(currentStation);
							for(Train train : trains) {
								if (train.line.name.equals(this.line.name)) {
									otherTrainsForLineAtStation = true;
									try {
										Simulation.trainsAtStations.wait();
									} catch (InterruptedException e) {
										e.printStackTrace();
									}
									break;
								}
							}
						}
					} while(otherTrainsForLineAtStation);
					event = SimulationEvent.trainArrival(this, currentStation);
					System.out.println(event.toString());
					System.out.flush();
					if (Simulation.trainsAtStations.containsKey(currentStation)) {
						trains = Simulation.trainsAtStations.get(currentStation);
					}
					trains.add(this);
					Simulation.trainsAtStations.put(currentStation, trains);
					Simulation.trainsAtStations.notifyAll();
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else {
				// Leaving a station
				synchronized(Simulation.trainsAtStations) {
					event = SimulationEvent.trainDeparture(this, currentStation);
					System.out.println(event.toString());
					System.out.flush();
					List<Train> trains = Simulation.trainsAtStations.get(currentStation);
					trains.remove(this);
					if (trains.isEmpty()) {
						Simulation.trainsAtStations.remove(currentStation);
					} else {
						Simulation.trainsAtStations.put(currentStation, trains);
					}
				}
				priorStation = currentStation;
				currentStation = null;
				active = areAllPassengersInactive(active, forward, priorStation);
			}
			
		} while(active);
	}

	private boolean areAllPassengersInactive(boolean active, boolean forward, Station priorStation) {
		if (!forward && line.indexOf(priorStation) == 0) {
			// Let's see if there are any passengers waiting at any stations
			boolean passengersWaiting = true;
			synchronized(Simulation.passAtStations) {
				passengersWaiting = !Simulation.passAtStations.isEmpty();
				//System.out.println("Number of stations with passengers waiting: " + Simulation.passAtStations.size() + " " + Simulation.passAtStations.toString());
			}
			
			// Let's see if there are any passengers still on any trains
			boolean passengersAboard = true;
			synchronized(Simulation.passOnTrains) {
			 	passengersAboard = !Simulation.passOnTrains.isEmpty();
			 	//System.out.println("Number of trains with passengers aboard: " + Simulation.passOnTrains.size() + " " + Simulation.passOnTrains.toString());
			}
			
			active = passengersWaiting || passengersAboard;
		}
		return active;
	}

	private boolean calculateTrainDirection(boolean forward, Station priorStation) {
		if (!forward && line.indexOf(priorStation) == 0) {
			forward = true;
		}
		if (forward && line.indexOf(priorStation) == (line.numStations() - 1)) {
			forward = false;
		}
		return forward;
	}
}
