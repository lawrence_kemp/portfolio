package us.kemp.lawrence.portfolio.metro;

public class SimulationEvent {
	public enum EventType {
		TrainArrival,
		TrainDeparture,
		PassengerBoard,
		PassengerAlight
	};
	
	public final EventType event;
	public final Train train;
	public final Station station;
	public final Passenger passenger;
	
	private SimulationEvent(EventType event, Train train, Station station, Passenger passenger) {
		this.event = event;
		this.train = train;
		this.station = station;
		if (train == null) {
			throw new Error("Bogus SimulationEvent: null train");
		}
		if (station == null) {
			throw new Error("Bogus SimulationEvent: null station");
		}
		this.passenger = passenger;
		if ((event == EventType.TrainArrival || event == EventType.TrainDeparture) && passenger != null) {
			throw new Error("Bogus SimulationEvent: non-null passenger for non-passenger event");			
		}
		if ((event == EventType.PassengerBoard || event == EventType.PassengerAlight) && passenger == null) {
			throw new Error("Bogus SimulationEvent: null passenger for passenger event");			
		}
	}
	
	public static SimulationEvent trainArrival(Train train, Station station) {
		return new SimulationEvent(EventType.TrainArrival, train, station, null);
	}
	
	public static SimulationEvent trainDeparture(Train train, Station station) {
		return new SimulationEvent(EventType.TrainDeparture, train, station, null);
	}
	
	public static SimulationEvent passengerBoard(Passenger passenger, Train train, Station station) {
		return new SimulationEvent(EventType.PassengerBoard, train, station, passenger);
	}
	
	public static SimulationEvent passengerAlight(Passenger passenger, Train train, Station station) {
		return new SimulationEvent(EventType.PassengerAlight, train, station, passenger);
	}

	@Override
	public String toString() {
		switch(event) {
		case TrainArrival:
			return "Train " + train + " entering " + station;
		case TrainDeparture:
			return "Train " + train + " leaving " + station;
		case PassengerBoard:
			return passenger + " boarding train " + train + " at " + station;
		case PassengerAlight:
			return passenger + " leaving train " + train + " at " + station;
		default:
			throw new Error("Illegal eent; can't be printed");
		}
	}
}
