# Metro Simulator

This is a metro train simulator application I'm using to test different thread manipulation styles in Java.

It was adapted from a University of Maryland, College Park CMSC 330 (Organiation of Programming Languages) project. I found this project at: <http://www.cs.umd.edu/class/spring2010/cmsc330/p5/>