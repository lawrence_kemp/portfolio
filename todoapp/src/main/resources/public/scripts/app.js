var app = angular.module( 'todoapp', [
    'ngCookies', 'ngResource', 'ngSanitize', 'ngRoute'
] );

app.config( function ( $routeProvider ) {
  $routeProvider.when( '/', {
    templateUrl : 'views/list.html',
    controller : 'ListCtrl'
  } ).when( '/:todoId', {
    templateUrl : 'views/edit.html',
    controller : 'EditCtrl'
  } ).when( '/create', {
    templateUrl : 'views/create.html',
    controller : 'CreateCtrl'
  } ).otherwise( {
    redirectTo : '/'
  } );
} );

app.controller( 'ListCtrl', function ( $scope, $http, $location ) {
  $http.get( '/api/v1/todos' ).success( function ( data ) {
    $scope.todos = data;
  } ).error( function ( data, status ) {
    console.log( 'Error ' + data );
  } );

  $scope.todoStatusChanged = function ( todo ) {
    console.log( todo );
    $http.put( '/api/v1/todos/' + todo.id.$oid, todo ).success( function ( data ) {
      console.log( 'status changed' );
    } ).error( function ( data, status ) {
      console.log( 'Error ' + data );
    } );
  };
  
  $scope.deleteTodo = function ( todo) {
    console.log( todo);
    $http.delete('/api/v1/todos/' + todo.id.$oid ).success( function (data) {
      $location.path( '/' );
    }).error( function (data, status) {
      console.log( 'Error ' + data );
    });
  };
} );

app.controller( 'EditCtrl', function ( $scope, $http, $routeParams, $location ) {
  $http.get( '/api/v1/todos' + $routeParams.todoId ).success( function ( data ) {
    $scope.todo = data;
  } ).error( function ( data, status ) {
    console.log( 'Error ' + data );
  } );

  $scope.editTodo = function ( todo ) {
    console.log( todo );
    $http.put( '/api/v1/todos/' + todo.id, todo ).success( function ( data ) {
      $location.path( '/' );
    } ).error( function ( data, status ) {
      console.log( 'Error ' + data );
    } );
  };
} );

app.controller( 'CreateCtrl', function ( $scope, $http, $location ) {
  $scope.todo = {
    done : false
  };

  $scope.createTodo = function () {
    console.log( $scope.todo );
    $http.post( '/api/v1/todos', $scope.todo ).success( function ( data ) {
      $location.path( '/' );
    } ).error( function ( data, status ) {
      console.log( 'Error ' + data );
    } );
  };
} );