package us.kemp.lawrence.portfolio.todoapp;

import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;
import static spark.Spark.delete;

/**
 * @author Lawrence
 *
 */
public final class TaskResource {

	/**
	 * The HTTP Status for Reset Content.
	 */
	private static final int HTTP_STATUS_RESET_CONTENT = 205;

	/**
	 * The HTTP Status for Created.
	 */
	private static final int HTTP_STATUS_CREATED = 201;

	/**
	 * 
	 */
	private static final String ID_PARAMETER = ":id";

	/**
	 * 
	 */
	private static final String ID_ROUTE_PARAMETER = "/:id";

	/**
	 * 
	 */
	private static final String TODOS_ROUTE = "/todos";

	/**
	 * 
	 */
	private static final String APPLICATION_JSON = "application/json";

	/**
	 * 
	 */
	private static final String API_CONTEXT = "/api/v1";

	/**
	 * 
	 */
	private final TaskService todoService;

	/**
	 * Instantiates an instance of the TaskResource class.
	 * 
	 * @param inputTaskService An instance of the TaskService.
	 */
	public TaskResource(final TaskService inputTaskService) {
		this.todoService = inputTaskService;
	}

	/**
	 * 
	 */
	public void setupEndpoints() {
		post(API_CONTEXT + TODOS_ROUTE, APPLICATION_JSON,
			(request, response) -> {
				todoService.createNewTodo(request.body());
				response.status(HTTP_STATUS_CREATED);
				return response;
			});

		get(API_CONTEXT + TODOS_ROUTE + ID_ROUTE_PARAMETER, APPLICATION_JSON,
				(request, response) -> 
					todoService.find(request.params(ID_PARAMETER)),
				new JsonTransformer());

		get(API_CONTEXT + TODOS_ROUTE, APPLICATION_JSON,
				(request, response) -> todoService.findAll(),
				new JsonTransformer());

		put(API_CONTEXT + TODOS_ROUTE + ID_ROUTE_PARAMETER, APPLICATION_JSON,
				(request, response) -> 
					todoService.update(request.params(ID_PARAMETER),
							request.body()),
				new JsonTransformer());

		delete(API_CONTEXT + TODOS_ROUTE + ID_ROUTE_PARAMETER, APPLICATION_JSON,
			(request, response) -> {
				todoService.delete(request.params(ID_PARAMETER));
				response.status(HTTP_STATUS_RESET_CONTENT);
				return response;
			});
	}
}
