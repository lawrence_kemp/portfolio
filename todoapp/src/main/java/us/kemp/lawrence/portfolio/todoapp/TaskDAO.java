package us.kemp.lawrence.portfolio.todoapp;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

/**
 * @author Lawrence
 *
 */
public final class TaskDAO extends BasicDAO<Task, ObjectId> {

	/**
	 * @param entityClass The class information for Task.
	 * @param ds The datastore for the application.
	 */
	public TaskDAO(final Class<Task> entityClass, final Datastore ds) {
		super(entityClass, ds);
	}
}
