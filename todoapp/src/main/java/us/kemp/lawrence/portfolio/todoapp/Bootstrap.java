package us.kemp.lawrence.portfolio.todoapp;

import static spark.Spark.ipAddress;
import static spark.Spark.port;
import static spark.Spark.staticFileLocation;

/**
 * @author Lawrence
 *
 */
public final class Bootstrap {

	/**
	 * The default port to use.
	 */
	private static final int LOCALPORT = 8080;
	
	/**
	 * The default IP address to use.
	 */
	private static final String LOCALHOST = "localhost";

	/**
	 * Creates an instance of the Bootstrap class.
	 * 
	 * This exists so that the class can never be externally instantiated.
	 */
	private Bootstrap() {
	}

	/**
	 * The starting point for the application.
	 * 
	 * @param args Arguments to be passed into the program.
	 * @throws Exception
	 * 
	 * At this point, there are no arguments for the program.
	 */
	public static void main(final String[] args) throws Exception {
		ipAddress(LOCALHOST);
		port(LOCALPORT);
		staticFileLocation("/public");
		
		TaskResource tr = new TaskResource(new TaskService(new MorphiaService()));
		tr.setupEndpoints();
	}
}
