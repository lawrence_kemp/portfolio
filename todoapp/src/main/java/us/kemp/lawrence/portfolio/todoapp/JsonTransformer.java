package us.kemp.lawrence.portfolio.todoapp;

import java.io.IOException;

import org.bson.types.ObjectId;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import spark.ResponseTransformer;

/**
 * @author Lawrence
 *
 */
public class JsonTransformer implements ResponseTransformer {

	/**
	 * 
	 */
	private Gson gson = new GsonBuilder().
			registerTypeAdapter(ObjectId.class,
					new ObjectIdTypeAdapter()).create();

	@Override
	public final String render(final Object model) throws Exception {
		return gson.toJson(model);
	}
	
	/**
	 * @author Lawrence
	 *
	 */
	private class ObjectIdTypeAdapter extends TypeAdapter<ObjectId> {

		@Override
		public void write(final JsonWriter out, final ObjectId value)
				throws IOException {
			out.beginObject().name("$oid").value(value.toString()).endObject();
		}

		@Override
		public ObjectId read(final JsonReader in) throws IOException {
			in.beginObject();
			String objectId = in.nextString();
			in.endObject();
			return new ObjectId(objectId);
		}
		
	}
}
