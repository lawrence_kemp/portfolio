package us.kemp.lawrence.portfolio.todoapp;

import java.util.List;

import org.bson.types.ObjectId;

import com.google.gson.Gson;

/**
 * @author Lawrence
 *
 */
public final class TaskService {

	/**
	 * 
	 */
	private MorphiaService morphiaService;
	/**
	 * 
	 */
	private TaskDAO todoDAO;
	
	/**
	 * @param inputMorphiaService An instance of the MorphiaService class.
	 */
	public TaskService(final MorphiaService inputMorphiaService) {
		this.morphiaService = inputMorphiaService;
		this.todoDAO = new TaskDAO(Task.class,
				this.morphiaService.getDatastore());
	}
	
	/**
	 * @return A list of Task objects.
	 */
	public List<Task> findAll() {
		return todoDAO.find().asList();
	}
	
	/**
	 * @param body The text of the Task.
	 */
	public void createNewTodo(final String body) {
		Task todo = new Gson().fromJson(body, Task.class);
		todoDAO.save(todo);
	}
	
	/**
	 * @param id The identifier of the Task.
	 * @return The Task object associated with the identifier.
	 */
	public Task find(final String id) {
		return todoDAO.get(new ObjectId(id));
	}
	
	/**
	 * @param todoId The identifier of the Task.
	 * @param body The contents of the Task object, in Json format.
	 * @return The updated Task object associated with the identifer. 
	 */
	public Task update(final String todoId, final String body) {
		Task todo = new Gson().fromJson(body, Task.class);
		todoDAO.save(todo);
		return this.find(todoId);
	}
	
	/**
	 * @param id The identifier of the Task.
	 */
	public void delete(final String id) {
		todoDAO.deleteById(new ObjectId(id));
	}
}
