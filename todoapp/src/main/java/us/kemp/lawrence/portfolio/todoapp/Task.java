package us.kemp.lawrence.portfolio.todoapp;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.bson.types.ObjectId;
import org.hibernate.validator.constraints.NotEmpty;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Index;
import org.mongodb.morphia.annotations.Indexes;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Version;

/**
 * @author Lawrence
 *
 */
@Entity(value = "tasks", noClassnameStored = true)
@Indexes({
	@Index("title")
})
public final class Task {

	/**
	 * 
	 */
	@Id
	private ObjectId id;
	
	/**
	 * 
	 */
	@Version
	private long version;
	
	/**
	 * 
	 */
	@Property
	@NotEmpty
	private String title;
	
	/**
	 * 
	 */
	@Property
	private boolean done;
	
	/**
	 * 
	 */
	@Property
	@NotNull
	private Date createdOn = new Date();

	/**
	 * @return The title of the task.
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * @return An indicator for noting if the task is complete.
	 */
	public boolean isDone() {
		return done;
	}
	
	/**
	 * @return The date the task was created.
	 */
	public Date getCreatedOn() {
		return createdOn;
	}
}
