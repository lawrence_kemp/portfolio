package us.kemp.lawrence.portfolio.todoapp;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.ValidationExtension;

import com.mongodb.MongoClient;

/**
 * @author Lawrence
 *
 */
public final class MorphiaService {

	/**
	 * 
	 */
	private Morphia morphia;
	/**
	 * 
	 */
	private Datastore datastore;

	/**
	 * 
	 */
	public MorphiaService() {
		MongoClient mongoClient = new MongoClient();

		this.morphia = new Morphia();
		this.morphia.map(Task.class);

		// This is a very weird pattern, as far as I'm concerned. Basically, 
		// what's happening is that the validation extension is being added to
		// the Morphia instance inside the constructor. It feels really odd
		// to me, but it works, so I'll allow it.
		new ValidationExtension(morphia); // NOSONAR
		String databaseName = "taskDatabase";
		this.datastore = morphia.createDatastore(mongoClient, databaseName);
	}

	/**
	 * @return The Morphia instance.
	 */
	public Morphia getMorphia() {
		return morphia;
	}

	/**
	 * @param inputMorphia The Morphia instance.
	 */
	public void setMorphia(final Morphia inputMorphia) {
		this.morphia = inputMorphia;
	}

	/**
	 * @return The datastore for the application.
	 */
	public Datastore getDatastore() {
		return datastore;
	}

	/**
	 * @param inputDatastore The datastore for the application.
	 */
	public void setDatastore(final Datastore inputDatastore) {
		this.datastore = inputDatastore;
	}
}
